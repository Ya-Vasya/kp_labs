package lab1.codejava;

public enum RecordGenre {
	Rock,
	Pop,
	Classic,
	Indie,
	LoFi,
	Rap
}

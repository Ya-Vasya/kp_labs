package lab1.codejava;

import java.util.Arrays;
import java.util.List;

public class Record {

	String _title;
	int _duration;
	String _album;
	double _price;
	Author _author;
	List<RecordGenre> _genres;
	
	public String get_title() {
		return _title;
	}

	public void set_title(String _title) {
		this._title = _title;
	}

	public int get_duration() {
		return _duration;
	}

	public void set_duration(int _duration) {
		this._duration = _duration;
	}

	public String get_album() {
		return _album;
	}

	public void set_album(String _album) {
		this._album = _album;
	}

	public double get_price() {
		return _price;
	}

	public void set_price(double _price) {
		this._price = _price;
	}

	public Author get_author() {
		return _author;
	}

	public void set_author(Author _author) {
		this._author = _author;
	}

	public List<RecordGenre> get_genres() {
		return _genres;
	}

	public void set_genres(List<RecordGenre> _genres) {
		this._genres = _genres;
	}
	
	public Record(String title, int dur, String album, double price, Author author, List<RecordGenre> genres)
	{
		if (title == null)
		{
			title = "FooBar";
		}
		if (dur == 0)
		{
			dur = 60;
		}
		if (album == null)
		{
			album = "Brralbum";
		}
		if (price == 0)
		{
			price = 1;
		}
		if (author == null) 
		{
			author = new Author(null, null);
		}
		if (genres == null) 
		{
			genres = Arrays.asList(new RecordGenre[]{RecordGenre.Pop});
		}
		_title = title;
		_duration = dur;
		_album = album;
		_price = price;
		_author = author;
		_genres = genres;
	}
	
	public void PlayRecord()
	{
		System.out.println("*music playing*");
	}
	
	private String getGenresString()
	{
		String result = "";
		for (var recordGenre : _genres) {
			result += (recordGenre.name().toString() + " ");
		}
		return result;
	}
	
	@Override
    public String toString() { 
        return String.format("*****\nTitle: " 
        		+_title.toString() + 
        		"\nAlbum: " + _album.toString() + 
        		"\nAuthor: " + _author._name + 
        		"\nDuration: " + _duration + 
        		"\nGenres: " + this.getGenresString() +
        		"\nPrice: " + _price); 
    } 
}

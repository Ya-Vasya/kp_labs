package lab1.codejava;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class RecordsManager {
	
	private List<Record> _records;

	public List<Record> get_records() {
		return _records;
	}

	public void set_records(List<Record> records) {
		this._records = records;
	}
	
	public void FindByGenre(RecordGenre genre)
	{
		System.out.println("\nSearchin by genre:" + genre.name());
		
		var res = _records.stream().filter(r -> r._genres.contains(genre))
				.collect(Collectors.toList());
		
		for (var record : res) {
			System.out.print(record.toString());
			System.out.println();
		}
	}
	
	public RecordsManager(List<Record> records)
	{
		if(records == null)
		{
			records = Arrays.asList(new Record[] {new Record(null, 0, null, 0, null, null)});
		}
		_records = records;
	}
	
	public void SortByPrice(boolean isAscending) 
	{
		System.out.println("\nSorting by price in " + (isAscending? "ascending" : "descending") + " order:");
		
		this._records.sort(PriceComparator.getInstatnse());
		if (!isAscending)
			Collections.reverse(_records);
	}
	
	public void SortByAuthor(boolean isAscending)
	{
		System.out.println("\nSorting by author name in " + (isAscending? "ascending" : "descending") + " order:");
		
		AuthorComparator authorComparator = new AuthorComparator();
		
		this._records.sort(authorComparator);
		if (!isAscending)
			Collections.reverse(_records);
	}
	
	public void SortByAlbum(boolean isAscending)
	{
		System.out.println("\nSorting by album name in " + (isAscending? "ascending" : "descending") + " order:");
		
		class MyAlbumComparator implements Comparator<Record>
		{
			@Override
			public int compare(Record arg0, Record arg1) {
				return arg0.get_album().compareToIgnoreCase(arg1.get_album());
			} 
		}
		
		var albumComparator = new MyAlbumComparator();
		
		this._records.sort(albumComparator);
		if (!isAscending)
			Collections.reverse(_records);
	}
    
    public static class PriceComparator implements Comparator<Record>
	{	
    	private static PriceComparator _priceComparator;
    	
    	private PriceComparator()
    	{
    		
    	}
    	
    	public static PriceComparator getInstatnse()
    	{
    		if (_priceComparator == null)
    			_priceComparator = new PriceComparator();
    		return _priceComparator;
    	}
    	
		public int compare(Record arg0, Record arg1) {
			return Double.compare(arg0.get_price(), arg1.get_price());
		}		
	}
	
	public class AuthorComparator implements Comparator<Record>
	{
		@Override
		public int compare(Record arg0, Record arg1) {
			return arg0.get_author().get_name().compareToIgnoreCase(arg1.get_author().get_name());
		}
	}
	
	public void Display() 
	{
		for (var record : _records) {
			System.out.print(record.toString());
			System.out.println("");
		}
	}
	
}

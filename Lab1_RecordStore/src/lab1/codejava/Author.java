package lab1.codejava;

import java.util.Arrays;
import java.util.List;

public class Author {
	
	String _name;
	List<String> _albums;
	
	public Author(String name, List<String> albums)
	{
		if (name == null)
		{
			name = "John Doe";
		}
		if (albums == null)
		{
			albums = Arrays.asList(new String[]{"foo", "bar in the jar"});
		}
		_name = name;
		_albums = albums;
	}
	
	public String get_name() {
		return _name;
	}
	public void set_name(String _name) {
		this._name = _name;
	}
	public List<String> get_albums() {
		return _albums;
	}
	public void set_albums(List<String> _albums) {
		this._albums = _albums;
	}
}

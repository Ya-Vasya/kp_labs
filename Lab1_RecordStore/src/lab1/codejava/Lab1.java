package lab1.codejava;

import java.util.Arrays;

public class Lab1 {

	public static void main(String[] args) 
	{
		Author den = new Author("Den", null);
		Record track1 = new Record("Beans", 3, den.get_albums().get(0), 1.25, den, null);
		Record track2 = new Record("Cool", 2, den.get_albums().get(0), 1.1, den, 
				Arrays.asList(new RecordGenre[] {RecordGenre.Indie, RecordGenre.LoFi}));
		
		Author aaron = new Author("Aaron", Arrays.asList(new String[] {"Simon and me", "Kitchen window"}));
		Record track3 = new Record("Sink", 2, aaron.get_albums().get(1), 2.50, aaron, 
				Arrays.asList(new RecordGenre[] {RecordGenre.Rap, RecordGenre.Pop}));
		Record track4 = new Record("Starring at me", 4, aaron.get_albums().get(1), 2, aaron, 
				Arrays.asList(new RecordGenre[] {RecordGenre.Indie, RecordGenre.LoFi}));

		RecordsManager recordsManager = new RecordsManager(Arrays.asList(new Record[] {track1, track2, track3, track4}));
		recordsManager.Display();
		
		recordsManager.SortByPrice(true);
		recordsManager.Display();
		
		recordsManager.SortByPrice(false);
		recordsManager.Display();
		
		recordsManager.SortByAlbum(true);
		recordsManager.Display();
		
		recordsManager.SortByAlbum(false);
		recordsManager.Display();
		
		recordsManager.SortByAuthor(true);
		recordsManager.Display();
		
		recordsManager.SortByAuthor(false);
		recordsManager.Display();

		recordsManager.FindByGenre(RecordGenre.LoFi);
	}

}

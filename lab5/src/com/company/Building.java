package com.company;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.util.*;
import java.util.List;

public class Building extends JPanel implements Runnable, Observer//like a monitor for threads?
{
    private Timer timer;
    private boolean isRunning;
    private int counter;
    private int timeElapsedInSecs;
    private Random random;

    private int refreshRate = 10;
    int floors;
    int minTime;

    ArrayList<Elevator> elevators;
    PassengerRandomizer randomizer;
    ArrayList<Thread> elevatorsThreads;
    Boolean isJoined = false;

    Building() {

    }

    public void start()
    {
        if (!isRunning)
        {
            isRunning = true;
        }
    }

    public void stop()
    {
        if (isRunning)
        {
            isRunning = false;
        }
    }




    Building(int floors, int elevatorsCount, int minTime) {

        this.isRunning = false;
        this.counter = 0;
        this.timeElapsedInSecs = 0;

        this.floors = floors;
        this.minTime = minTime;
        randomizer = new PassengerRandomizer(minTime, floors);//set randomizer
        elevators = new ArrayList<>();

        /*
        for(int i = 0; i < elevatorsCount; ++i) {
            elevators.add(new Elevator(100 + 150 * i, 400,500, new FirstStrategy(), floors));
        }
        */
        elevators.add(new Elevator(0, 100, 400, 500, new SecondStrategy(), floors));
        elevators.add(new Elevator(1, 250, 400, 500, new FirstStrategy(), floors));

        counter = 0;
        random = new Random();

        Timer timer = new Timer(10, (e) -> {

            counter += refreshRate;

            if (counter / 1000 == 1) {
                counter = 0;
                ++timeElapsedInSecs;

            }

            if (isRunning) {
                repaint();
            }
        });

        timer.start();
    }

    public void stopWork() throws InterruptedException {
        Logger.log("Building stop working");

        for (Elevator elevator: elevators)
        {
            elevator.SetHasToStop(true);
        }

        for (Thread thread: elevatorsThreads)
        {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    public void run() {
        Logger.log("Building start work");
        isRunning = true;

        elevatorsThreads = new ArrayList<>();
        int i = 0;
        for (Elevator elevator : elevators) {
            Thread elevatorThread = new Thread(elevator, "ElevatorThread" + i);
            elevatorThread.start();
            elevatorsThreads.add(elevatorThread);
            i++;
        }

        for(;;){

            //here random generate passenger
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            Passenger passenger = randomizer.getRandomizedPassenger();
            int queueNumber = getIndexOfMinPassangersQueueOnFloor(passenger.getSourceFloor());
            try {
                    elevators.get(queueNumber).AddPassengerToQueue(passenger.getSourceFloor()-1, passenger);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            if(isJoined)
            {
                try {
                    stopWork();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                break;
            }
        }
    }

    private int getIndexOfMinPassangersQueueOnFloor(int floor) {
        int minSize = elevators.get(0).getPassengersCountFromQueue(floor - 1);
        int minSizeIndex = 0;
        for (int i = 0; i < elevators.size(); ++i) {
            int size = elevators.get(i).getPassengersCountFromQueue(floor - 1);
            if (size < minSize) {
                minSize = size;
                minSizeIndex = i;
            }
        }
        return minSizeIndex;
    }

    public boolean getIsJoined() {
        return isJoined;
    }

    public void setJoined(boolean joined) {
        isJoined = joined;
    }

    @Override
    public void update(Observable o, Object isJoined)
    {
        this.setJoined((boolean)isJoined);
    }

    public void drawFloor(Graphics g, int ii) {

        int x1 = 100;
        int x2 = 200;
        int x3 = 250;
        int x4 = 350;
        int y = 100 * (5 - ii);
        int l = 750;

        g.drawLine(0, y, x1, y);
        g.drawLine(x2, y, x3, y);
        g.drawLine(x4, y, l, y);

        g.setColor(Color.LIGHT_GRAY);

        g.fillRect(222, y - 50, 8, 10);
        g.fillRect(372, y - 50, 8, 10);

        g.drawLine(x1, y, x2, y);
        g.drawLine(x3, y, x4, y);

        g.setColor(Color.BLACK);

        g.drawString("Floor " + (ii + 1), 30, y - 45);

        int queueCount = 0;

        for (Elevator el : elevators) {
            Iterator iterator = el.getQueues().get(ii).iterator();

            while (iterator.hasNext())
            {
                Passenger temp = (Passenger) iterator.next();

                temp.setY(y - 50);

                if (temp.getMode() == Passenger.Mode.WAIT) {
                    temp.setX(400 + queueCount * 20);
                    queueCount++;
                }

                temp.draw(g);
            }

            el.draw(g);

        }

    }



    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        this.setBackground(Color.WHITE);

        drawFloor(g, 0);
        drawFloor(g, 1);
        drawFloor(g, 2);
        drawFloor(g, 3);
        drawFloor(g, 4);
       /* for (Floor floor : floors)
        {
            floor.draw(g);
        }*/
        for (Elevator el : elevators) {
            el.draw(g);
        }
    }






    /*public Hashtable<Integer, ArrayList<Passenger>> getTotalPassengers()
    {
        int i = 0;
        Hashtable<Integer, ArrayList<Passenger>> table = new Hashtable<>();

        for(int i = 0; i < 5; i++)
        {
            ArrayList<Passenger> ps = new ArrayList<>();
            for (Elevator el : elevators
            ) {
                ps.addAll(el.getQueues().get(i));
            }

            table.put(i, ps);
        }
    }*/
}

package com.company;

public abstract class BaseStrategy implements Strategy {
    public int pickupPassengers(Elevator elevator)
    {
        int wasCount = elevator.getPassengers().size();

        elevator.GetPassengersFromQueue();

        int newCount = elevator.getPassengers().size();
        Logger.log("Elevator " + elevator.getIndex() + " at floor " + elevator.getFloor() +
                " is picking up passengers. Was: " + wasCount + " now: " + newCount);

        return newCount - wasCount;
    }

    public int deletePassengers(Elevator elevator)
    {
        int wasCount = elevator.getPassengers().size();

        elevator.DeletePassengers();

        int newCount = elevator.getPassengers().size();
        Logger.log("Elevator " + elevator.getIndex() + " at floor " + elevator.getFloor() +
                " is releasing passengers. Was: " + wasCount + " now: " + newCount);

        return wasCount - newCount;
    }

    public abstract void changeFloor(Elevator elevator);
}

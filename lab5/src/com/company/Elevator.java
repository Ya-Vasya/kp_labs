package com.company;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;

public class Elevator implements Runnable
{
    private int xAxis;
    private int yAxis;
    private int width;
    private int height;
    private int doorWidth;
    private boolean isOpen;
    private boolean isClose;

    private Passenger.Mode mode;

    private int index;
    private int maxWeight;
    private Directions direction;
    private int floor;
    private Strategy strategy;
    private ArrayList<Passenger> passengers;
    private boolean isAvailable;
    private Hashtable<Integer, ArrayList<Passenger>> queues;
    private boolean HasToStop = false;

    private PropertyChangeSupport support;

    public Elevator(int index, int xAxis, int yAxis, int maxWeight, Strategy strategy, int floorCount)
    {
        this.xAxis = xAxis;
        this.yAxis = yAxis;
        this.width = 100;
        this.height = 100;
        floor = 1;

        this.index = index;
        this.maxWeight = maxWeight;
        this.strategy = strategy;
        direction = Directions.Up;
        this.doorWidth = width / 2;
        this.isOpen = false;
        this.isClose = true;
        this.mode = Passenger.Mode.WAIT;

        passengers = new ArrayList<>();
        support = new PropertyChangeSupport(this);
        queues = new Hashtable<>();
        InitialiseFloors(floorCount);
        HasToStop = false;
    }

    // for Observable
    public void addPropertyChangeListener(PropertyChangeListener pcl) {
        support.addPropertyChangeListener(pcl);
    }

    public void removePropertyChangeListener(PropertyChangeListener pcl) {
        support.removePropertyChangeListener(pcl);
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Logger.log("Elevator " + index + " started");
        while (true)
        {
            strategy.execute(this);

            AtomicInteger sum = new AtomicInteger();
            sum.addAndGet(passengers.size());
            queues.forEach((x, y) -> sum.addAndGet(y.size()));
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if(sum.get() == 0 && HasToStop)
            {
                break;
            }
        }
        Logger.log("Elevator " + index + " stopped");
    }

    public void SetHasToStop(boolean hasToStop)
    {
        HasToStop = hasToStop;
    }

    private void InitialiseFloors(int floorCount)
    {
        for (int i = 0; i < floorCount; i++)
        {
            queues.put(i, new ArrayList<>());
        }
    }

    public void AddPassengerToQueue(int floor, Passenger passenger) throws Exception {
        if(floor > queues.size())
        {
            throw new Exception("Floor number is bigger than flor count in building");
        }

        this.queues.get(floor).add(passenger);
    }

    // getters and setters
    public int getIndex() { return index; }

    public int getMaxWeight() { return maxWeight; }

    public void setMaxWeight(int maxWeight) { this.maxWeight = maxWeight; }

    public Directions getDirection() { return direction; }

    public void setDirection(Directions direction) { this.direction = direction; }

    public int getFloor() { return floor; }

    public void setFloor(int floor) {
        support.firePropertyChange("floor", this.floor, floor);
        this.floor = floor;
        setY(100 * (5 - floor));
    }

    public Strategy getStrategy() { return strategy; }

    public void setStrategy(Strategy strategy)
    {
        this.strategy = strategy;
    }

    public Hashtable<Integer, ArrayList<Passenger>> getQueues() {
        return queues;
    }

    public boolean getIsAvailable()
    {
        isAvailable = maxWeight <= passengers.stream().mapToInt(Passenger::getWeight).sum();
        return isAvailable;
    }

    public ArrayList<Passenger> getPassengers() {
        return passengers;
    }

    public int getFloorCount(){
        return queues.size();
    }

    private void AllignPassangers() {
        int i = 0;
        for (Passenger passenger : passengers) {
            passenger.setMode(Passenger.Mode.WAIT);
            passenger.setX(this.xAxis + i * passenger.weight * 2);
            i++;
        }
    }

    public void GetPassengersFromQueue()
    {
        int diff = maxWeight - passengers.stream().mapToInt(Passenger::getWeight).sum();
        while(diff > 0)
        {
            int finalDiff = diff;
            Passenger passenger = this.queues
                    .get(floor - 1)
                    .stream()
                    .filter(x -> x.getWeight() < finalDiff)
                    .findFirst()
                    .orElse(null);

            if(passenger == null)
            {
                break;
            }

            passengers.add(passenger);
            diff -= passenger.weight;
            this.queues.get(floor - 1).remove(passenger);
        }

        AllignPassangers();
    }

    public int getPassengersCountFromQueue(int floor)
    {
        return queues.get(floor).size();
    }

    public synchronized void DeletePassengers(){
        for (Iterator<Passenger> iter = passengers.iterator(); iter.hasNext();) {
            Passenger ps = iter.next();
            if (ps.getDestinationFloor() == this.floor) {
                iter.remove();
            }
        }

        AllignPassangers();
    }

    public void draw(Graphics g)
    {

        g.drawRect(xAxis, yAxis, width, height);
        g.setColor(Color.WHITE);
        g.fillRect(xAxis, yAxis, doorWidth, height);
        g.fillRect(xAxis + width - doorWidth, yAxis, doorWidth, height);
        g.setColor(Color.BLACK);
        g.drawRect(xAxis, yAxis, doorWidth, height);
        g.drawRect(xAxis + width - doorWidth, yAxis, doorWidth, height);

        for (int i = 0, j = xAxis; i < passengers.size(); i++, j += 15)
        {
            Passenger passenger = passengers.get(i);
            passenger.setX(j);
            passenger.setY(yAxis + height - passenger.getHeight());
            passenger.draw(g);
        }
    }

    public void step() {
        switch (mode) {
            case UP:

                --yAxis;

                for (Passenger passenger : passengers) {
                    passenger.setY(passenger.getY() - 1);
                }

                if (yAxis % 100 == 0) {
                    ++floor;

                    if (floor == 4) {
                        direction = Directions.Down;
                    }
                }

                break;

            case DOWN:

                ++yAxis;

                for (Passenger passenger : passengers) {
                    passenger.setY(passenger.getY() + 1);
                }

                if (yAxis % 100 == 0) {
                    --floor;

                    if (floor == 0) {
                        direction = Directions.Up;
                    }
                }

                break;

            case OPEN:

                if (doorWidth > 0) {
                    --doorWidth;
                } else if (doorWidth == 0) {
                    isOpen = true;
                    isClose = false;
                }

                break;

            case CLOSE:

                if (doorWidth < width / 2) {
                    ++doorWidth;
                } else if (doorWidth == width / 2) {
                    isOpen = false;
                    isClose = true;
                }

                break;

            case WAIT:


            default:

                break;
        }
    }


    public int getY() {
        return this.yAxis;
    }

    public void setMode(Passenger.Mode m)
    {
        this.mode = m;
    }

    public Passenger.Mode getMode()
    {
        return mode;
    }

    public int getX() {
        return this.xAxis;
    }

    public void setY(int y) {
        this.yAxis = y;
    }
}





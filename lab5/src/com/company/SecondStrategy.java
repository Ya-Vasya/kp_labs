package com.company;

import java.util.ArrayList;
import java.util.Hashtable;


public class SecondStrategy extends FirstStrategy {
    @Override
    public void execute(Elevator elevator) {
        while (true) {
            if (state == 0) {
                state = 1;
                if (elevator.getPassengers().size() == 0) {
                    if (pickupPassengers(elevator) != 0) {
                        return;
                    }
                }
            }

            if (state == 1) {
                changeFloor(elevator);
                state = 2;
                return;
            }

            if (state == 2) {
                state = 0;
                if (deletePassengers(elevator) != 0) {
                    return;
                }
            }
        }
    }
}

/*
public class SecondStrategy extends BaseStrategy implements Strategy {

    private int current_floor;
    private Directions directions;

    @Override
    public void execute(Elevator elevator) {
        current_floor = elevator.getFloor();
        directions = elevator.getDirection();

        pickupPassengers(elevator);
        changeFloor(elevator);
        deletePassengers(elevator);
    }

    private void optimalChangeFloor(Elevator e) {
        int next_floor = Integer.MAX_VALUE;
        if(hasPassengers(e)){
            next_floor = getNearestDestination(e.getPassengers(), e);
            if(hasAnyWaitingBetweenFloors(next_floor, e.getQueues())){
                next_floor = getNearestDestinationForPickUp(next_floor, e.getQueues());
            }
        }
        else if(hasAnyWaiting(e.getQueues())){
            next_floor = getNearestPickUpDestination(e);
        }
        else{
            next_floor = getNextFloor(e); //todo wait for new passengers
        }
        next_floor = next_floor;

        e.setFloor(next_floor );
    }

    private int getNextFloor(Elevator e) {
        if(e.getDirection() == Directions.Up
                && current_floor == e.getFloorCount())
        {
            directions = Directions.Down;
        }
        else if(e.getDirection() == Directions.Down
                && current_floor == 1)
        {
            directions = Directions.Up;
        }
        e.setDirection(directions);
        switch (directions){
            case Up:
                return current_floor + 1;
            case Down:
                return current_floor - 1;
            default:
                return current_floor;
        }
    }

    private int getNearestPickUpDestination(Elevator e) {
        var queues = e.getQueues();
        int next_floor = Integer.MAX_VALUE;
        int diff = Integer.MAX_VALUE;
        for(var entry : queues.entrySet()){
            if(entry.getValue().size() > 0
                    && Math.abs(current_floor - entry.getKey()) < diff
                //&& Math.abs(current_floor - entry.getKey()) != 0
            )
            {
                diff = Math.abs(current_floor - entry.getKey());
                next_floor = entry.getKey();
            }
        }

        if(next_floor > current_floor) {
            e.setDirection(Directions.Up);
        }
        else if(next_floor < current_floor){
            e.setDirection(Directions.Down);
        }

        return next_floor + 1;
    }

    private boolean hasAnyWaiting(Hashtable<Integer, ArrayList<Passenger>> queues) {
        for(var entry : queues.entrySet()){
            if(entry.getValue().size() > 0)
                return true;
        }
        return false;
    }

    private int getNearestDestinationForPickUp(int next_floor, Hashtable<Integer, ArrayList<Passenger>> queues) {
        int new_destination;
        switch (directions){
            case Up:
                new_destination = Integer.MAX_VALUE;
                for(var entry : queues.entrySet()){
                    if(entry.getKey() > current_floor
                            && entry.getKey() < next_floor
                            && entry.getValue().size() > 0
                            && entry.getKey() < new_destination)
                    {
                        new_destination = entry.getKey();
                    }

                }
                break;
            case Down:
                new_destination = Integer.MIN_VALUE;
                for(var entry : queues.entrySet()){
                    if(entry.getKey() < current_floor
                            && entry.getKey() > next_floor
                            && entry.getValue().size() > 0
                            && entry.getKey() > new_destination)
                    {
                        new_destination = entry.getKey();
                    }
                }
                break;
            default:
                return next_floor;
        }
        return new_destination;
    }

    private boolean hasAnyWaitingBetweenFloors(int next_floor, Hashtable<Integer, ArrayList<Passenger>> queues) {
        switch (directions){
            case Up:
                for(var entry : queues.entrySet()){
                    if(entry.getKey() > current_floor
                            && entry.getKey() < next_floor
                            && entry.getValue().size() > 0)
                        return true;
                }
                break;
            case Down:
                for(var entry : queues.entrySet()){
                    if(entry.getKey() < current_floor
                            && entry.getKey() > next_floor
                            && entry.getValue().size() > 0)
                        return true;
                }
                break;
            default:
                return false;
        }
        return false;
    }

    private int getNearestDestination(ArrayList<Passenger> passengers, Elevator e) {
        int next_destination;
        switch (directions){
            case Up :
                next_destination = Integer.MAX_VALUE;
                for(Passenger p : passengers) {
                    if (p.getDestinationFloor() > current_floor
                            && p.getDestinationFloor() < next_destination)
                    {
                        next_destination = p.getDestinationFloor();
                    }
                }
                if(next_destination == Integer.MAX_VALUE){
                    directions = Directions.Down;
                    e.setDirection(directions);
                    return getNearestDestination(passengers, e);
                }
                break;
            case Down:
                next_destination = Integer.MIN_VALUE;
                for(Passenger p : passengers){
                    if(p.getDestinationFloor() < current_floor
                            && p.getDestinationFloor() > next_destination)
                    {
                        next_destination = p.getDestinationFloor();
                    }
                }
                if(next_destination == Integer.MIN_VALUE){
                    directions = Directions.Up;
                    e.setDirection(directions);
                    return getNearestDestination(passengers, e);
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + directions);
        }
        return next_destination;
    }

    private boolean hasPassengers(Elevator e) {
        return e.getPassengers().size() > 0;
    }

    @Override
    public void changeFloor(Elevator e) {
        int max_floor = e.getFloorCount();
        if (current_floor == max_floor && e.getDirection() == Directions.Up)
            e.setDirection(Directions.Down);
        if (current_floor == 1 && e.getDirection() == Directions.Down)
            e.setDirection(Directions.Up);
        switch (e.getDirection()) {
            case Up:
                e.setFloor(current_floor + 1);
                break;
            case Down:
                e.setFloor(current_floor - 1);
                break;
        }
    }

}
*/

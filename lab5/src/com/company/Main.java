package com.company;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Observable;

public class Main extends Observable {

    private final JFrame frame;
    private final JPanel panel;
    private final JButton btnStart;
    private final JButton btnStop;
    //private World world;
    //private Building building;
    private Thread buildingThread;
    private boolean isBuildingJoined = false;

//    public void main(String[] args)
//    {
//        Logger.initEventLogger("log.txt");
//        int floors = 5;//sets in UI
//        int minTime = 1000;//ms, sets in UI
//         Building building = new Building(floors,2, minTime);//elevators and floors count set in UI
//        Thread buildingThread = new Thread(building,"BuildingThread");
//        buildingThread.start();
//        try {
//            buildingThread.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println("Main thread finished");
//
//        Logger.saveLogs();
//
//        //world = new World();
//        //world.setBounds(0, 0, 750, 500);
//
//        btnStart = new JButton("Start");
//        btnStart.setBounds(290, 512, 80, 25);
//        btnStart.setFocusPainted(false);
//        btnStart.addActionListener((e) -> {
//            world.start();
//            System.out.println("pressed start");
//        });
//
//        btnStop = new JButton("Stop");
//        btnStop.setBounds(380, 512, 80, 25);
//        btnStop.setFocusPainted(false);
//        btnStop.addActionListener((e) -> {
//            world.stop();
//            System.out.println("pressed stop");
//        });
//
//        panel = new JPanel(null);
//        panel.setPreferredSize(new Dimension(750, 550));
//        panel.add(world);
//        panel.add(btnStart);
//        panel.add(btnStop);
//
//        frame = new JFrame("Elevator Simulator");
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        frame.setAlwaysOnTop(true);
//        frame.setResizable(false);
//        frame.add(panel);
//        frame.pack();
//        frame.setVisible(true);
//    }

    public void setBuildingJoined(boolean isBuildingJoined)
    {
        this.isBuildingJoined = isBuildingJoined;
        setChanged();
        notifyObservers(this.isBuildingJoined);
    }

    public Main()
    {
        Logger.initEventLogger("log.txt");
        int floors = 5;//sets in UI
        int minTime = 1000;//ms, sets in UI
        Building building = new Building(floors,2, minTime);//elevators and floors count set in UI
        this.addObserver(building);
        building.setBounds(0, 0, 750, 500);
        buildingThread = new Thread(building,"BuildingThread");
        //world = new World();
        building.setBounds(0, 0, 750, 500);

        btnStart = new JButton("Start");
        btnStart.setBounds(290, 512, 80, 25);
        btnStart.setFocusPainted(false);
        btnStart.addActionListener((e) -> {
            buildingThread.start();
            System.out.println("pressed start");
//            try {
//                buildingThread.join();
//            } catch (InterruptedException exp) {
//                exp.printStackTrace();
//            }
            //System.out.println("Main thread finished");

            //Logger.saveLogs();
        });

        btnStop = new JButton("Stop");
        btnStop.setBounds(380, 512, 80, 25);
        btnStop.setFocusPainted(false);
        btnStop.addActionListener((e) -> {
            //world.stop();
            setBuildingJoined(true);
            /*
            try {
                //buildingThread.join();
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
            */

            System.out.println("pressed stop");
            System.out.println(buildingThread.getState());
            System.out.println("MAIN THREAD FINISHED");
        });

        panel = new JPanel(null);
        panel.setPreferredSize(new Dimension(750, 550));
        panel.add(building);
        panel.add(btnStart);
        panel.add(btnStop);

        frame = new JFrame("Elevator Simulator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setAlwaysOnTop(true);
        frame.setResizable(false);
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);

//        int floors = 5;//sets in UI
//        int minTime = 1000;//ms, sets in UI
        //Building building = new Building(floors,2, minTime);//elevators and floors count set in UI
        //Thread buildingThread = new Thread(building,"BuildingThread");
        //buildingThread.start();




    }
    public static void main(String[] args)
    {
        Main main = new Main();
    }
}

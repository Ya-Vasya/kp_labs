package com.company;
import java.awt.*;


public class Passenger implements Runnable
{
    private int xAxis;
    private int yAxis;
    private int width;
    private int height;

    int id;
    int weight;
    private int sourceFloor;
    private int destinationFloor;
    private int destinationPosX;
    private int currentFloor;
    private Mode mode;

    @Override
    public void run() {

    }
    Passenger(int xAxis, int yAxis, int id, int weight, int src, int dest){

        this.xAxis = xAxis;
        this.yAxis = yAxis;
        this.width = 10;
        this.height = 50;
        this.id = id;
        this.weight = weight;
        this.destinationFloor = dest;
        this.sourceFloor = src;
        this.mode = Mode.LEFT;
        Logger.log("id: " + id + " Floor: " + sourceFloor + " I'm spawned! Destination: " + destinationFloor);
    }

    public int getCurrentFloor()
    {
        return this.currentFloor;
    }
    public int getSourceFloor()
    {
        return this.sourceFloor;
    }
    public int getDestinationFloor()
    {
        return this.destinationFloor;
    }
    public int getWeight()
    {
        return this.weight;
    }
    public int getHeight() { return this.height; }
    public Mode getMode() { return this.mode; }
    public void setMode(Mode mode) {
        this.mode = mode;
    }

    public enum Mode {UP, DOWN, WAIT, RIGHT, LEFT, OPEN, CLOSE};

    public void setX(int x)
    {
        this.xAxis = x;
    }
    public void setY(int y)
    {
        this.yAxis = y;
    }
    public void setDestPosX(int x)
    {
        this.destinationPosX = x;
    }
    public int getX()
    {
        return this.xAxis;
    }
    public int getY()
    {
        return this.yAxis;
    }
    public int getDestPosX()
    {
        return this.destinationPosX;
    }

    public void draw(Graphics g)
    {
        g.fillRect(xAxis, yAxis, width, height);
        g.drawString("W: " + weight, xAxis - 10, yAxis - 31);
        g.drawString("S: " + sourceFloor, xAxis - 5, yAxis - 18);
        g.drawString("D: " + destinationFloor, xAxis - 5, yAxis - 5);

        if (xAxis == destinationPosX)
        {
            mode = Mode.WAIT;
        }

        step();
    }

    public void step()
    {
        switch (mode)
        {
            case LEFT:
                --xAxis;
                break;
            case RIGHT:
                ++xAxis;
                break;
            case UP:
                --yAxis;
                break;
            case DOWN:
                ++yAxis;
                break;
            default:
                break;
        }
    }

}

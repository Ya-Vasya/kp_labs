package com.company;

import java.util.Hashtable;
import java.util.concurrent.ThreadLocalRandom;

public class PassengerRandomizer
{
    int minTime;
    int maxFloor;
    int id = 0;
    Hashtable<Integer, Passenger> passengers;
    PassengerRandomizer(int minTime, int maxFloor){
        this.minTime = minTime;
        this.maxFloor = maxFloor;
    }

    Passenger getRandomizedPassenger(){
        ++id;
        int sourceFloor = randomFloor(0);
        Passenger passenger = new Passenger(700, 100 * (5 - sourceFloor) - 50, id,  randomWeight(), sourceFloor, randomFloor(sourceFloor));
        passenger.setDestPosX(500);
        return passenger;
    }

    private int randomWeight(){//todo
        return ThreadLocalRandom.current().nextInt(40, 90);
    }

    private int randomFloor(int source){//todo
        int dest = source;
        while (source == dest)
            dest = ThreadLocalRandom.current().nextInt(1, maxFloor + 1);
        return dest;
    }
}

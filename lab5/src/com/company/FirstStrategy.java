package com.company;

import java.util.Iterator;

public class FirstStrategy extends BaseStrategy
{
    int state = 0;
    @Override
    public void execute(Elevator elevator) {
        while (true) {
            if (state == 0) {
                state = 1;
                if (pickupPassengers(elevator) != 0) {
                    return;
                }
            }

            if (state == 1) {
                changeFloor(elevator);
                state = 2;
                return;
            }

            if (state == 2) {
                state = 0;
                if (deletePassengers(elevator) != 0) {
                    return;
                }
            }
        }
    }

    public void changeFloor(Elevator elevator)
    {
        if(elevator.getPassengers().size() != 0)
        {
            int current_floor = elevator.getFloor();
            int nextFloor = elevator.getPassengers().get(0).getDestinationFloor();
            int distance = Math.abs(nextFloor - current_floor);

            for (Passenger ps: elevator.getPassengers()) {
                int nextFloor2 = ps.getDestinationFloor();
                int dist = Math.abs(nextFloor2 - current_floor);
                if (dist < distance) {
                    distance = dist;
                    nextFloor = nextFloor2;
                }
            }
            int step = (nextFloor - current_floor) / distance; // moving 1 floor at a time
            //Logger.log("moving step=" + step);
            elevator.setFloor(current_floor + step);
        }
        else {
            int current_floor = elevator.getFloor();
            int max_floor = elevator.getFloorCount();
            if (current_floor == max_floor && elevator.getDirection() == Directions.Up)
                elevator.setDirection(Directions.Down);
            if (current_floor == 1 && elevator.getDirection() == Directions.Down)
                elevator.setDirection(Directions.Up);
            switch (elevator.getDirection()) {
                case Up:
                    elevator.setFloor(current_floor + 1);
                    break;
                case Down:
                    elevator.setFloor(current_floor - 1);
                    break;
            }
        }
    }

}


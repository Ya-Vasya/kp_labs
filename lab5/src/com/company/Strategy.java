package com.company;

public interface Strategy {
    void execute(Elevator elevator);
}

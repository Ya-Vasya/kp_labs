package Lab3;

import java.io.FileNotFoundException;

public class Laboratory3 {

	public static void main(String[] args) throws FileNotFoundException {
		Manager mngr = new Manager();
		mngr.ReadData();
		mngr.DisplayDates();
		mngr.AddValuesToDates();
		mngr.ChangeSentences();
	}

}

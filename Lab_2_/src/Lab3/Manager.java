package Lab3;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.StringJoiner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Manager {
	String file = "TextData.txt";
	List<String> words;
	List<Date> dates;
	List<String> formatStrings = Arrays.asList("d/M/y", "d-M-y");
	
	public Manager()
	{
		words = new ArrayList<String>();
		dates = new ArrayList<Date>();
	}
	
	public Date tryParse(String dateString)
	{
	    for (String formatString : formatStrings)
	    {
	        try
	        {
	            return new SimpleDateFormat(formatString).parse(dateString);
	        }
	        catch (ParseException e) {}
	    }
	    return null;
	}
	
	public void ReadData() throws FileNotFoundException
	{
			Scanner reader = new Scanner(new File(file));
			while(reader.hasNextLine())
			{
	            String data = "";
	            data = reader.nextLine();
	            String[] elem = data.split("\\s+");
	            for(var a: elem)
	            {
	            	words.add(a);
	            }
	        }
			reader.close();
			
			for (var word: words)
				dates.add(this.tryParse(word));
			dates = dates.stream().filter(Objects::nonNull).collect(Collectors.toList());
	}
	
	public void AddValuesToDates()
	{
		var formatter = new SimpleDateFormat("d-M-y (E)");
		for (var date: dates)
		{
			Calendar c = Calendar.getInstance();
			c.setTime(date);
			c.add(Calendar.DAY_OF_MONTH, 1);
			date = c.getTime();
			System.out.println(formatter.format(date));
		}
	}
	
	public void DisplayDates()
	{
		var formatter = new SimpleDateFormat("d-M-y");
		for (var date: dates)
		{
			System.out.println(formatter.format(date));
		}
	}
	
	//--------------------------------------------------
	// Add I have a birthday today
	public void ChangeSentences()
	{
		String data = "";
		List<String> sentences = new ArrayList<String>();
		Scanner reader = new Scanner(System.in);
		data = reader.nextLine();
		String[] arr = data.split("[.]");
		for(var a: arr)
        {
        sentences.add(a);
        }
		for(var sent: sentences)
		{
			List<String> words = Arrays.asList(sent.split("\\s+"));
			String longest = words.stream().max(Comparator.comparingInt(String::length)).orElse(null);
			Pattern pattern = Pattern.compile("[aeiouy]*", Pattern.CASE_INSENSITIVE);
			Matcher m = pattern.matcher(sent);
			String vowelWord = m.group();
			int i = words.indexOf(longest);
			int j = words.indexOf(vowelWord);
			Collections.swap(words, i, j);
			
			StringJoiner joiner = new StringJoiner("");
		      for(var w: words) {
		         joiner.add(w);
		      }
		      String str = joiner.toString();
		      System.out.println(str);
		}
		reader.close();
	}
}

package Lab;

import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;

public class NewspaperManager 
{
	private String[] files = new String[] {"data.txt", "data2.txt"};
	public Map<String, List<String>> dNPapers;
	public List<Newspaper> newspapers;
	public final List<Newspaper> finNews;
	
	public NewspaperManager() throws FileNotFoundException
	{
		newspapers = new ArrayList<Newspaper>();
		dNPapers = new HashMap<String, List<String>>();
		
		ReadData();
		SetMap();
		finNews = getReadnlyCollection();
	}
	
	public void ReadData() throws FileNotFoundException
	{
		for (String file : files) 
		{
			Scanner reader = new Scanner(new File(file));
			while(reader.hasNextLine())
			{
	            String data = "";
	            data = reader.nextLine();
	            String[] elem = data.split(" ");
	            newspapers.add(GetData(elem));
	        }
			reader.close();
		}
	}
	
	private void SetMap()
	{
		for (Newspaper p : newspapers) {
			if (!dNPapers.containsKey(p.get_publisher())) {
			    List<String> list = new ArrayList<String>();
			    list.add(p.get_name());

			    dNPapers.put(p.get_publisher(), list);
			} else {
				dNPapers.get(p.get_publisher()).add(p.get_name());
			}
		}
	}
	
	
	public void ChangeOddAndEven_DeleteWhereLessThan(int count)
	{	
		for(Iterator<Map.Entry<String, List<String>>> it = dNPapers.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, List<String>> entry = it.next();
            
            entry.getValue().add(0, entry.getValue().get(entry.getValue().size()-1));
            entry.getValue().remove(entry.getValue().size()-1);
		}
		
		Map<String, List<String>> result = dNPapers.entrySet()
			      .stream()
			      .filter(map -> map.getValue().size() > count)
			      .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
		
		Display(result);
	}
	
	public void DisplayMintageFrequensy()
	{
			var res = newspapers.stream().collect(Collectors.groupingBy(Newspaper::get_mintage));
			for(Iterator<Map.Entry<Integer, List<Newspaper>>> it = res.entrySet().iterator(); it.hasNext(); ) {
	            Map.Entry<Integer, List<Newspaper>> entry = it.next();
	            
	            System.out.println(entry.getKey() +": " + entry.getValue().size());
			}
	}
	
	private List<Newspaper> getReadnlyCollection()
	{
		List<Newspaper> readonlyNews = newspapers;
		readonlyNews.sort(Comparator.comparing(Newspaper::get_name).reversed());
		return readonlyNews;
	}
	
	private Newspaper GetData(String[] arr)
	{
		Newspaper nPaper = new Newspaper();
		nPaper.set_name(arr[0]);
		nPaper.setfDate(Integer.parseInt(arr[1]));
		nPaper.set_mintage(Integer.parseInt(arr[2]));
		nPaper.set_frequencyPerMonth(Integer.parseInt(arr[3]));
		nPaper.set_publisher(arr[4]);
		return nPaper;
	}
	
	// Helping methods
	
	@Override
	public String toString()
	{
		String result = "";
		
		for (Map.Entry<String, List<String>> pair: dNPapers.entrySet())
		{
			result += pair.getKey() + ": \n";
			for (String name: pair.getValue())
			{
				result += "\t" + name;
			}
			result += "\n";
		}
		result += "\n";
		return result;
	}
	
	public void DisplayReadonlyList()
	{
		for(Newspaper p: finNews)
		{
			System.out.print("\n*********************\n" + 
					"Name: " + p.get_name() 
					+ "\nFoundation date: " + p.getfDate() 
					+ "\nMintage: " + p.get_mintage() 
					+ "\nFrequency: " + p.get_frequencyPerMonth()
					+ "\nPublisher: " + p.get_publisher());
		}
	}
	
	public void Display(Map<String, List<String>> map)
	{
		String result = "";
		
		for (Map.Entry<String, List<String>> pair: map.entrySet())
		{
			result += pair.getKey() + ": \n";
			for (String name: pair.getValue())
			{
				result += "\t" + name;
			}
			result += "\n";
		}
		result += "\n";
		System.out.print(result);
	}
	
}

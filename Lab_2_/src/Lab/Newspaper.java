package Lab;

public class Newspaper 
{
	
	private String _name;
	private int fDate;
	private int _mintage;
	private int _frequencyPerMonth;
	private String _publisher;
	
	public String get_name() {
		return _name;
	}
	public void set_name(String _name) {
		this._name = _name;
	}
	public int getfDate() {
		return fDate;
	}
	public void setfDate(int fDate) {
		this.fDate = fDate;
	}
	public int get_mintage() {
		return _mintage;
	}
	public void set_mintage(int _mintage) {
		this._mintage = _mintage;
	}
	public int get_frequencyPerMonth() {
		return _frequencyPerMonth;
	}
	public void set_frequencyPerMonth(int _frequencyPerMonth) {
		this._frequencyPerMonth = _frequencyPerMonth;
	}
	public String get_publisher() {
		return _publisher;
	}
	public void set_publisher(String _publisher) {
		this._publisher = _publisher;
	}
	
}

class FoundationDate
{	
	public int Year;
	public int Month;
	public int Day;

	public FoundationDate(int year, int month, int day)
	{
		Year = year;
		Month = month;
		Day = day;
	}
}

import javax.swing.*;

public class Main {

    public static void main(String[] args)
    {
        ThreadManager mngr = new ThreadManager("C://Windows", 0, "^[Mm].*");
        JFrame.setDefaultLookAndFeelDecorated(true);
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                GUIBuilder builder = new GUIBuilder(mngr);
                builder.createGUI();
            }
        });
    }
}
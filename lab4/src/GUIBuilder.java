import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class GUIBuilder {

    private JFrame frame;
    private ThreadManager mngr;
    private ThreadListener lsnr = new ThreadListener();

    public GUIBuilder(ThreadManager mngr) {
        frame = new JFrame("Thread logger");
        this.mngr = mngr;
    }

    public void createGUI()
    {
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setPreferredSize(new Dimension(650, 355));

        JTextField input = new JTextField("4");
        input.setBounds(105, 10, 70, 20);

        JLabel labelForInput = new JLabel("Thread amount:");
        labelForInput.setBounds(10, 10, 100, 20);
        labelForInput.setLabelFor(input);

        JTextArea memo = new JTextArea();
        memo.setBounds(190, 10, 440, 300);
        memo.setEditable(false);

        JScrollPane scroll = new JScrollPane(memo);
        scroll.setBounds(190, 10, 440, 300);

        JButton btn = new JButton("Run");
        btn.setBounds(55, 80, 65, 25);
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mngr.Run(Integer.parseInt(input.getText()));
                UpdateCombo(mngr.GetThreadNames());
                ResultWaiter waiter = new ResultWaiter(mngr.GetThreads());
                waiter.start();
            }
        });

        JComboBox combo = new JComboBox();
        combo.setBounds(105, 40, 70, 20);
        combo.addActionListener(new ActionListener() {
            Timer tmr = new Timer("Timer");
            @Override
            public void actionPerformed(ActionEvent e) {
                tmr.purge();
                lsnr.AttachThread(mngr.GetThread(combo.getSelectedItem().toString()));
                memo.setText("");
                TimerTask tmrtask = new TimerTask(){
                    public void run() {
                        memo.setText(lsnr.GetInfo());
                    }
                };
                tmr.scheduleAtFixedRate(tmrtask, 0,100);
            }
        });

        JLabel labelForCombo = new JLabel("Trace thread:");
        labelForCombo.setBounds(10, 40, 100, 20);
        labelForCombo.setLabelFor(combo);

        JPanel panel = new JPanel();
        panel.setLayout(null);

        panel.add(labelForInput);
        panel.add(input);
        panel.add(labelForCombo);
        panel.add(combo);
        panel.add(scroll);
        panel.add(btn);

        frame.getContentPane().add(panel);

        frame.pack();
        frame.setVisible(true);
    }

    public void UpdateCombo(ArrayList<String> threads) {
        Component[] comp = ((JPanel)frame.getContentPane().getComponent(0)).getComponents();
        for (int i=0; i<comp.length; ++i) {
            if(comp[i] instanceof JComboBox) {
                JComboBox combo = (JComboBox)comp[i];
                for(String thread : threads) combo.addItem(thread);
            }
        }
    }
}

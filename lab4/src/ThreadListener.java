public class ThreadListener {
    private Thread thread;

    public Thread GetThread() {
        return thread;
    }

    public void AttachThread(Thread newThread) {
        thread = newThread;
    }

    public String GetInfo() {
        return "Name-"+thread.getName()+" State-"+thread.getState()+" Priority-"+thread.getPriority()+"\n";
    }
}

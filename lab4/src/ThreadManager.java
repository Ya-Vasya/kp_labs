import java.io.File;
import java.util.ArrayList;

public class ThreadManager {
    private String folderName;
    private ArrayList<RepoAnalyser> threads;
    private int minimalSize;
    private String regex;

    public ThreadManager(String folderName, int minimalSize, String regex) {
        this.folderName = folderName;
        threads = new ArrayList<RepoAnalyser>();
        this.minimalSize = minimalSize;
        this.regex = regex;
    }

    public RepoAnalyser GetThread(String name) {
        for (RepoAnalyser thread : threads) {
            if(thread.getName()==name) return thread;
        }
        // return threads.stream().filter(x -> x.getName() == name).findFirst().get();
        return null;
    }

    public ArrayList<RepoAnalyser> GetThreads() {
        return threads;
    }

    public ArrayList<String> GetThreadNames() {
        ArrayList<String> res = new ArrayList<String>();
        for (RepoAnalyser thread : threads) res.add(thread.getName());
        return res;
    }

    public void Run(int threadCount) {
        File[] dir = new File(folderName).listFiles();
        int filesPerThread = dir.length/threadCount;
        int begin = 0, end = filesPerThread;
        for(int i=0; i<threadCount; ++i) {
            threads.add(new RepoAnalyser("Thread No"+(int)(i+1), begin, end, dir, minimalSize, regex));
            begin = end;
            end += filesPerThread;
            if(i == filesPerThread-2) end = dir.length;
        }
        for (RepoAnalyser thread : threads) {
            thread.start();
        }
    }
}

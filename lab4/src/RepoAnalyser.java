import java.io.File;

public class RepoAnalyser extends Thread {
    private int beginIndex;
    private int endIndex;
    private File[] filesList;
    private int minimalSize;
    private String regex;
    private Results res;

    RepoAnalyser(String name, int beginIndex, int endIndex, File[] filesList, int minimalSize, String regex) {
        super(name);
        this.beginIndex = beginIndex;
        this.endIndex = endIndex;
        this.filesList = filesList;
        this.minimalSize = minimalSize;
        this.regex = regex;
        res = new Results();
    }

    public Results GetResult() {
        return res;
    }

    public void run() {
        for(int i = beginIndex; i<endIndex; ++i) {
            if(filesList[i].getTotalSpace() > minimalSize) res.countOfBiggerFiles++;
            if(filesList[i].getName().matches(regex)) res.countOfMatchingFiles++;
            if(filesList[i].isDirectory()) {
                String out = currentThread().getName()+". Sub folder: "+filesList[i].getName()+". Size="+
                        filesList[i].getTotalSpace();
                if(filesList[i].listFiles()!=null) out += ";\nSub files - "+filesList[i].listFiles().length;
                System.out.println(out);
                res.countOfSubFolders++;
            } else {
                System.out.println(currentThread().getName()+". File: " +filesList[i].getName()+
                        filesList[i].getName()+". Size="+ filesList[i].getTotalSpace());
            }
            try {
                sleep(200);
            } catch (InterruptedException exc) {
                System.out.println(exc.getMessage());
            }
        }
    }
}

public class Results {
    public int countOfBiggerFiles;
    public int countOfMatchingFiles;
    public int countOfSubFolders;

    public Results() {
        countOfBiggerFiles = 0;
        countOfMatchingFiles = 0;
        countOfSubFolders = 0;
    }

    public void Add(Results res){
        countOfMatchingFiles += res.countOfMatchingFiles;
        countOfBiggerFiles += res.countOfBiggerFiles;
        countOfSubFolders += res.countOfSubFolders;
    }

    @Override
    public String toString() {
        return "Count of bigger files: "+countOfBiggerFiles+"\n"+
                "Count of matching files: "+countOfMatchingFiles+"\n"+
                "Count of sub folders: "+countOfSubFolders;
    }
}

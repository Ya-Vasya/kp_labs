import java.util.ArrayList;

public class ResultWaiter extends Thread {
    private ArrayList<RepoAnalyser> threads;

    public ResultWaiter(ArrayList<RepoAnalyser> threads) {
        this.threads = threads;
    }

    public void run() {
        for (RepoAnalyser thread : threads) {
            try{
                thread.join();
            }
            catch(InterruptedException exc) {
                System.out.printf(exc.getMessage());
            }
        }
        Results generalResult = new Results();
        for (RepoAnalyser thread : threads) {
            generalResult.Add(thread.GetResult());
        }
        System.out.println(generalResult.toString());
    }
}
